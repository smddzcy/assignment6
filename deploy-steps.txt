echo "This is the deploy step"

export PROJECT_ID=cs378-assignment5-201320
gcloud config set project $PROJECT_ID
export CLOUDSDK_COMPUTE_ZONE=us-central1-b

gcloud container clusters get-credentials app

kubectl delete deployment app-deployment || echo "app-deployment deployment does not exist"
kubectl delete service app-deployment || echo "app-deployment service does not exist"
kubectl delete ingress app-ingress || echo "app-ingress does not exist"

kubectl create -f deployment.yaml
kubectl expose deployment app-deployment --target-port=5000 --type=NodePort

kubectl apply -f ingress.yaml

echo "Done deploying"
