FROM ubuntu:14.04
MAINTAINER Samed Duzcay "samedduzcay@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python-setuptools python-pip
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "movies_web_app.py"]