import os

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    """
    Reads the DB name, username, password, and hostname from the environment variables
    and returns them in a tuple.
    """
    db = os.environ.get("DB_NAME", None)
    username = os.environ.get("DB_USERNAME", None)
    password = os.environ.get("DB_PASSWORD", None)
    hostname = os.environ.get("DB_HOSTNAME", None)
    return db, username, password, hostname

def get_db_cnx():
    """
    Returns the DB connection object.
    """
    db, username, password, hostname = get_db_creds()

    cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    return cnx

def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    table_ddl = """
        CREATE TABLE movies(
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            year INT UNSIGNED,
            title VARCHAR(255) NOT NULL UNIQUE,
            director TEXT,
            actor TEXT,
            release_date TEXT,
            rating FLOAT(4,2),
            PRIMARY KEY (id)
        )
    """

    cnx = get_db_cnx()
    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("Already exists.")
        else:
            print(err.msg)


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    print(request.form)
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    cnx = get_db_cnx()

    try:
        cur = cnx.cursor()
        query = "INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES (%(year)s, %(title)s, %(director)s, %(actor)s, %(release_date)s, %(rating)s)"
        cur.execute(query, {'year': year, 'title': title, 'director': director, 'actor': actor, 'release_date': release_date, 'rating': rating})
        cnx.commit()

        cur.close()
        cnx.close()

        return hello("Movie %s successfully inserted" % title)
    except Exception as exp:
        return hello("Movie %s could not be inserted - %s" % (title, exp))

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    print(request.form)
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = get_db_cnx()

    try:
        cur = cnx.cursor()
        query = "SELECT id FROM movies WHERE title = %(title)s"
        cur.execute(query, {'title': title})

        if len(cur.fetchall()) == 0:
            cur.close()
            cnx.close()
            # There is no such movie with the given title
            return hello("Movie with %s does not exist" % title)
        else:
            # Movies exists
            cur = cnx.cursor()
            query = "UPDATE movies SET year = %(year)s, director = %(director)s, actor = %(actor)s, release_date = %(release_date)s, rating = %(rating)s WHERE title = %(title)s"
            cur.execute(query, {'year': year, 'director': director, 'actor': actor, 'release_date': release_date, 'rating': rating, 'title': title})
            cnx.commit()

            cur.close()
            cnx.close()

        return hello("Movie %s successfully updated" % title)
    except Exception as exp:
        return hello("Movie %s could not be updated - %s" % (title, exp))

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    print(request.form)
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = get_db_cnx()

    try:
        cur = cnx.cursor()
        query = "SELECT id FROM movies WHERE title = %(title)s"
        cur.execute(query, {'title': title})

        if len(cur.fetchall()) == 0:
            cur.close()
            cnx.close()
            # There is no such movie with the given title
            return hello("Movie with %s does not exist" % title)
        else:
            cur = cnx.cursor()
            query = "DELETE FROM movies WHERE title = %(title)s"
            cur.execute(query, {'title': title})
            cnx.commit()

            cur.close()
            cnx.close()

        return hello("Movie %s successfully deleted" % title)
    except Exception as exp:
        return hello("Movie %s could not be deleted - %s" % (title, exp))

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    print(request.args)
    actor = request.args.get('search_actor', "")

    db, username, password, hostname = get_db_creds()

    cnx = get_db_cnx()

    try:
        cur = cnx.cursor()
        query = "SELECT title, year, actor FROM movies WHERE actor = %(actor)s"
        cur.execute(query, {'actor': actor})

        response = ["<{}, {}, {}>".format(title, year, actor) for (title, year, actor) in cur]
        cur.close()
        cnx.close()
        if len(response) == 0:
            return hello("No movies found for actor %s" % actor)
        else:
            return hello("\n".join(response))
    except Exception as exp:
        return hello("Movies for actor %s could not be searched - %s" % (actor, exp))

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = get_db_cnx()

    try:
        cur = cnx.cursor()
        query = "SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT rating FROM movies ORDER BY rating DESC LIMIT 1)"
        cur.execute(query)

        response = ["<{}, {}, {}, {}, {}>".format(title, year, actor, director, rating) for (title, year, actor, director, rating) in cur]
        cur.close()
        cnx.close()

        if len(response) == 0:
            return hello("There is no movie")
        else:
            return hello("\n".join(response))
    except Exception as exp:
        return hello("Highest rating movie could not be found")

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = get_db_cnx()

    try:
        cur = cnx.cursor()
        query = "SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT rating FROM movies ORDER BY rating ASC LIMIT 1)"
        cur.execute(query)

        response = ["<{}, {}, {}, {}, {}>".format(title, year, actor, director, rating) for (title, year, actor, director, rating) in cur]
        cur.close()
        cnx.close()

        if len(response) == 0:
            return hello("There is no movie")
        else:
            return hello("\n".join(response))
    except Exception as exp:
        return hello("Lowest rating movie could not be found")


@app.route("/")
def hello(msg=""):
    return render_template('index.html', message=msg)


if __name__ == "__main__":
    create_table()
    app.debug = True
    app.run(host='0.0.0.0')